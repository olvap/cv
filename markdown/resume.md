Oldani Pablo
============

----

> Pablo is a full stack web developer with over 15 years of experience working
> with Ruby on Rails and Javascript technologies like React.js and Redux. Pablo
> has spent the last years of his career developing web applications for
> national and international clients. Pablo is incredibly organized and knows
> how to put priorities in the right place, he is very interested in the
> technology field, making it his passion and profession.

> Pablo is a proactive, emphatic, responsible and reliable employee, he is
> always looking to accomplish all his tasks successfully and help his
> teammates with support and guidance. Throughout his career, he has managed to
> absorb countless amounts of soft and tech skills from every project he has
> participated in.

----

Skill Set
---------

* Ruby, Ruby on Rails
* JavaScript (Vue, ReactJS)
* HTML5 / CSS3 / SASS / SCSS
* JSON, APIs
* Single Page Applications (SPAs), Universal JavaScript Libraries, User Interface Frameworks, Styleguides
* MySQL, PostgreSQL, Redis
* TDD, BDD
* OOP, MVC
* Unix (Linux), SSH, Git, Vim Editor, Chrome Dev Tools, NPM, Webpack, Babel
* Git, Workflows
* Agile Methodologies, Scrum

Experience
----------

2021 - Present
:   **Q3digital**
_Full Stack Engineer_
Full stack developer, working with Vue.js and React.js.

2019 - 2020
:   **Clockwork**
_Full Stack RubyOnRails Developer_
Developed and maintained APIs in Ruby on Rails for a recruiting company.

2015 - 2019
:   **ThinkCerca**
_Full Stack Javascript/Ruby Developer_
Developed e-learning web applications using Ruby on Rails and ReactJS.

2012 - 2015
:   **Altoros S.A.**
_Full Stack RubyOnRails Developer_
Worked with international clients, using JavaScript, HTML, CSS, MQTT, and ReactJS.

2007 - 2012
:   **Municipalidad de la ciudad de Coronda**
_Full Stack Developer_
Developed administrative web applications for Argentine county management and control using Ruby on Rails and JavaScript.

Education
---------

2010-2014
:   Engineering, Informatics; Universidad Nacional del Litoral (Santa Fe)

2000-2008
:   Engineering, Mechanica; Universidad Tecnológica Nacional (Santa Fe)

Extra
-----

* Human Languages:
    * Spanish (native speaker)
    * English (Upper intermediate)

* Preferred Editor: **Vim**

* Preferred OS: **Linux**

----
-
-> <oldani.pablo@gmail.com> • +54 (342) 5097578 \
-> Irigoyen Freyre 2232 5A - Santa Fe, Argentina

